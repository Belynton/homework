package ru.egartech.systemObjects;

public abstract class BaseObjects {
    // название системы
    String nameSystem;
    // код документа по классификатору
    String codeTypeDocument;
    // название документа
    String nameDocument;
    // количество страниц документа
    Integer countPage;

    public Register initRegister() {
        var register = new Register();
        register.setNameSystem(nameSystem);
        register.setCodeTypeDocument(CodeTypesDictionary.getNameByCode(codeTypeDocument));
        return register;
    }

    // маркер объекта
    public abstract String getMarker();

    public abstract Register processFields();
}
