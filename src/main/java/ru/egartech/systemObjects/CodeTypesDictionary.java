package ru.egartech.systemObjects;

import java.util.HashMap;
import java.util.Map;

public class CodeTypesDictionary {

    private static final Map<String, String> codes = new HashMap<>() {{
        put("1001", "Справка о количестве архивных документов категрии А");
        put("2001", "Справка о количестве обработанных пакетов из центрального архива");
        put("2002", "Справка о количестве полученных пакетов из центрального архива");
        put("2003", "Справка о времени обработки полученных пакетов из центрального архива");
        put("3001", "Справка о статистике за сентябрь 2023 г.");
    }};

    public static String getNameByCode(String code) {
        var name = codes.get(code);
        return name != null ? name : String.format("Не найдено название документа для кода %s", code);
    }
}
