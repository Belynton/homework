package ru.egartech.systemObjects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IdentityDocumentDictionary {

    private static final Map<String, String> codes = new HashMap<>() {{
        put("10", "Паспорт иностранного гражданина");
        put("12", "Вид на жительство");
        put("14", "Временное удостоверение личности гражданина Российской Федерации");
        put("21", "Паспорт гражданина Российской Федерации");
        put("23", "Свидетельство о рождении, выданное уполномоченным органом иностранного государства");
    }};

    public static String getNameByCode(String code) {
        var name = codes.get(code);
        return name != null ? name : "Не определен";
    }

    public static List<String> getKeys() {
        return codes.keySet().stream().toList();
    }
}
