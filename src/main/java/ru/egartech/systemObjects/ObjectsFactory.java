package ru.egartech.systemObjects;

import java.time.LocalDateTime;
import java.util.Random;

public class ObjectsFactory {
    public BaseObjects createObject(Integer codeSystem) {
        switch (codeSystem) {
            case 1:
                return new objectFromSystemA(
                        "1",
                        "info1_ObjectSystemA",
                        "info1_ObjectSystemA"
                );
            case 2:
                return new objectFromSystemB(LocalDateTime.parse(
                        "2023-08-06T00:00:00"),
                        "info1_ObjectSystemB",
                        "info2_ObjectSystemB",
                        "info3_ObjectSystemB"
                );
            case 3:
                return new objectFromSystemC(generateRandomIdentityCode(),
                        "info1_ObjectSystemB",
                        "info2_ObjectSystemB",
                        "info3_ObjectSystemB"
                );
            default:
                throw new RuntimeException("нет данных для обработки");
        }

    }

    private String generateRandomIdentityCode() {
        var keys = IdentityDocumentDictionary.getKeys();
        var randomCode = keys.get(new Random().nextInt(keys.size()));
        return String.valueOf(randomCode);
    }
}
