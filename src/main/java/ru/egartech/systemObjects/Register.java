package ru.egartech.systemObjects;

public class Register {

    private String nameSystem;
    private String codeTypeDocument;
    private String identityDocumentName = "Не определен";
    private Boolean isOutdated = false;
    private String systemStatus = "Не определен";

    public String getIdentityDocumentName() {
        return identityDocumentName;
    }

    public void setIdentityDocumentName(String identityDocumentName) {
        this.identityDocumentName = identityDocumentName;
    }

    public Boolean getOutdated() {
        return isOutdated;
    }

    public void setOutdated(Boolean outdated) {
        isOutdated = outdated;
    }

    public String getSystemStatus() {
        return systemStatus;
    }

    public void setSystemStatus(String systemStatus) {
        this.systemStatus = systemStatus;
    }

    public String getNameSystem() {
        return nameSystem;
    }

    public void setNameSystem(String nameSystem) {
        this.nameSystem = nameSystem;
    }

    public String getCodeTypeDocument() {
        return codeTypeDocument;
    }

    public void setCodeTypeDocument(String codeTypeDocument) {
        this.codeTypeDocument = codeTypeDocument;
    }

    @Override
    public String toString() {
        return "Register{" +
                "nameSystem='" + nameSystem + '\'' +
                ", codeTypeDocument='" + codeTypeDocument + '\'' +
                ", identityDocumentName='" + identityDocumentName + '\'' +
                ", isOutdated=" + isOutdated +
                ", systemStatus='" + systemStatus + '\'' +
                '}';
    }
}
