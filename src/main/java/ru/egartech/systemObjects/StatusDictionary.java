package ru.egartech.systemObjects;

import java.util.HashMap;
import java.util.Map;

public class StatusDictionary {

    private static final Map<String, String> codes = new HashMap<>() {{
        put("1", "Обработан");
        put("2", "Обрабатывается");
        put("3", "Ошибка при обработке");
        put("4", "В очереди на обработку");
    }};

    public static String getNameByCode(String code) {
        var name = codes.get(code);
        return name != null ? name : "Неизвестный статус обработки";
    }
}
