package ru.egartech.systemObjects;

import java.time.LocalDateTime;

/**
 * Региональный центр обработки данных
 */
public class objectFromSystemB extends BaseObjects {

    private LocalDateTime receiveTime;
    private String info1_ObjectSystemB;
    private String info2_ObjectSystemB;
    private String info3_ObjectSystemB;

    public objectFromSystemB() {
        nameSystem = "System B";
        codeTypeDocument = "2001";
        nameDocument = "Справка о количестве обработанных пакетов из центрального архива";
        countPage = 28;
    }

    public objectFromSystemB(LocalDateTime receiveTime,
                             String info1_ObjectSystemB,
                             String info2_ObjectSystemB,
                             String info3_ObjectSystemB) {
        this();
        this.receiveTime = receiveTime;
        this.info1_ObjectSystemB = info1_ObjectSystemB;
        this.info2_ObjectSystemB = info2_ObjectSystemB;
        this.info3_ObjectSystemB = info3_ObjectSystemB;
    }

    @Override
    public String getMarker() {
        return "objectFromSystemB";
    }

    @Override
    public Register processFields() {
        var register = initRegister();
        // если время получения данного документа превышает 1 месяц, то считаем его устаревшим
        register.setOutdated(this.receiveTime.isBefore(LocalDateTime.now().minusMonths(1L)));
        return register;
    }

    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getInfo1_ObjectSystemB() {
        return info1_ObjectSystemB;
    }

    public void setInfo1_ObjectSystemB(String info1_ObjectSystemB) {
        this.info1_ObjectSystemB = info1_ObjectSystemB;
    }

    public String getInfo2_ObjectSystemB() {
        return info2_ObjectSystemB;
    }

    public void setInfo2_ObjectSystemB(String info2_ObjectSystemB) {
        this.info2_ObjectSystemB = info2_ObjectSystemB;
    }

    public String getInfo3_ObjectSystemB() {
        return info3_ObjectSystemB;
    }

    public void setInfo3_ObjectSystemB(String info3_ObjectSystemB) {
        this.info3_ObjectSystemB = info3_ObjectSystemB;
    }
}
