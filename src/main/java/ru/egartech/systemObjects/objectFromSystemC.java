package ru.egartech.systemObjects;

/**
 * Служба занятости населения
 */
public class objectFromSystemC extends BaseObjects {


    private String identityDocumentCode;
    private String info1_ObjectSystemC;
    private String info2_ObjectSystemC;
    private String info3_ObjectSystemC;

    public objectFromSystemC() {
        nameSystem = "System C";
        codeTypeDocument = "3001";
        nameDocument = "Справка о статистике за сентябрь 2023 г.";
        countPage = 2;
    }

    public objectFromSystemC(String identityDocumentCode,
                             String info1_ObjectSystemC,
                             String info2_ObjectSystemC,
                             String info3_ObjectSystemC) {
        this();
        this.identityDocumentCode = identityDocumentCode;
        this.info1_ObjectSystemC = info1_ObjectSystemC;
        this.info2_ObjectSystemC = info2_ObjectSystemC;
        this.info3_ObjectSystemC = info3_ObjectSystemC;
    }

    @Override
    public String getMarker() {
        return "objectFromSystemC";
    }

    @Override
    public Register processFields() {
        var register = initRegister();
        register.setIdentityDocumentName(IdentityDocumentDictionary.getNameByCode(identityDocumentCode));
        return register;
    }

    public String getIdentityDocument() {
        return identityDocumentCode;
    }

    public void setIdentityDocument(String identityDocumentCode) {
        this.identityDocumentCode = identityDocumentCode;
    }

    public String getInfo1_ObjectSystemC() {
        return info1_ObjectSystemC;
    }

    public void setInfo1_ObjectSystemC(String info1_ObjectSystemC) {
        this.info1_ObjectSystemC = info1_ObjectSystemC;
    }

    public String getInfo2_ObjectSystemC() {
        return info2_ObjectSystemC;
    }

    public void setInfo2_ObjectSystemC(String info2_ObjectSystemC) {
        this.info2_ObjectSystemC = info2_ObjectSystemC;
    }

    public String getInfo3_ObjectSystemC() {
        return info3_ObjectSystemC;
    }

    public void setInfo3_ObjectSystemC(String info3_ObjectSystemC) {
        this.info3_ObjectSystemC = info3_ObjectSystemC;
    }
}
